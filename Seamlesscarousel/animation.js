function animation(obj,target,callback) {
	clearInterval(obj.timer);
	obj.timer = setInterval(function(){
		let speed = (target-obj.offsetLeft)/10;
		speed = speed > 0 ? Math.ceil(speed):Math.floor(speed);
		if (obj.offsetLeft === target) {
			clearInterval(obj.timer);
			callback && callback();
		} else {
			obj.style.left = obj.offsetLeft+speed+'px';
		}
	},15)
}